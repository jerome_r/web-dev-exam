CSS and HTML and JS

---All colors should be in hex or rgba()

-Code and style based on this wireframe https://wireframe.cc/N7Bcwl 
--The size of the container should be 1200.
--The Main Block(in Black fill) should be 70% of the total width
--The Sidebar Block should be 30%
--The height of the header and footer should be 300 each.
--Fill the header and footer with background color on each block.(any color will do)

-After coding the wireframe
--Add an overlay background on top of the wireframe with opacity 0.6, with background color of black.
--Add an box with a size of 500 x 300 on top of the overlay
--the box should be centered and fixed

-After working on the Pop-up box
--Hide it.
--Then create a button any where on the page with a JS script when clicked, it will show the pop-up box
--Add an JS script in the background of the pop-up where in when clicked it will fade out the pop-box
--And add a JS script where after closing the pop-box, after 3 seconds, it will fade back in again

-After working on all task above
--Make the page responsive along the pop-up box function
---------------------------------------------------------------------------------------------------

Creare A simple calculator using CSS, HTML and JS
The app should have Addition, Subtraction, Division and Multiplication functions

--------------------------------------------

--PHP

Write a PHP program to reverse the digits of an integer.

Sample :
x = 234, return 432
x = -234, return -432

--------------------------

Instructions:

Create a simple fighting game simulator between yourself and one AI enemy.

The game is simple, you both start off with 100 health each and both of you will take turns choosing 
which moves to do in order to defeat the other. The game is over once either one has health below 0 and then a winner will be declared.

To start:

1. The player will be prompted to input his/her name.
2. The game will begin with both the player and the computer having 100 health.
3. Let the game auto generate a name for your enemy.
	* Create an array of names and give the enemy a name randomly.
4. Let the game announce both names. PLAYER_NAME V.S. ENEMY_NAME
5. Prompt the user to choose a move.
	* [ Attack - Heal - Special Attack ]
6. Depending on what the users selects as a move the following will be the affect:
	ATTACK - Reduce 10 health from enemy
	HEAL - Restore 8 health to yourself you cannot heal more than 100.
	SPECIAL ATTACK - Reduce 25 health from enemy
7. After choosing a move, the enemy will also respond
	ATTACK - Reduce 10 health from player
	HEAL - Restore 8 health to enemy
	SPECIAL ATTACK - Reduce 25 health to player
	DO NOTHING - Does nothing
8. The enemy's move is always random.
9. Announce what moves were made:
	ex. "Player used attack, enemy used heal."
10. Once one of you has health below 0 announce the winner and stop the game.

Apply Object Oriented Programming.

--------------------------

Write a PHP program to sort a list of elements using Insertion sort.

Insertion sort is a simple sorting algorithm that builds the final sorted array (or list) one item at a time. It is much less efficient on large lists than more advanced algorithms such as quicksort, heapsort, or merge sort.

https://www.awesomescreenshot.com/image/3625572/61ea57dba5c96cc264f00bba01eab9f4

--------------------------

Write a PHP program to sort a list of elements using Bubble sort.

According to Wikipedia "Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the list to be sorted, compares each pair of adjacent items and swaps them if they are in the wrong order. The pass through the list is repeated until no swaps are needed, which indicates that the list is sorted. The algorithm, which is a comparison sort, is named for the way smaller elements "bubble" to the top of the list. Although the algorithm is simple, it is too slow and impractical for most problems even when compared to insertion sort. It can be practical if the input is usually in sort order but may occasionally have some out-of-order elements nearly in position."

https://www.awesomescreenshot.com/image/3625571/e9a8f7f163e2fbf26278516581c1582d

--------------------------

Write a PHP script to find the maximum and minimum marks from the following set of arrays.

Sample arrays :
$marks1 = array(360,310,310,330,313,375,456,111,256);
$marks2 = array(350,340,356,330,321);
$marks3 = array(630,340,570,635,434,255,298);

--------------------------

https://forms.gle/PSsFYSH3SinALszZ6